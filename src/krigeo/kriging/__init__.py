# -*- coding: utf-8 -*-
"""Kriging interpolators"""

# from .simple import SimpleKriging  # for reading/learning purpose only, don't expose
from .ordinary import OrdinaryKriging
from .universal import UniversalKriging
