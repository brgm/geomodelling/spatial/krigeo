# -*- coding: utf-8 -*-
from ..fault import Fault
from .vertical import VerticalFault

models = {
    "vertical": VerticalFault,
}
