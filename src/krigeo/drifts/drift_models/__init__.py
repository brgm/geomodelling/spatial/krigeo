# -*- coding: utf-8 -*-

from ..drift import Drift
from .gaussian import GaussianDrift
from .heaviside import HeavisideDrift

models = {
    "heaviside": HeavisideDrift,
    "gaussian": GaussianDrift,
}
