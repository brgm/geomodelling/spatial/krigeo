# -*- coding: utf-8 -*-
import numpy as np
from numpy.typing import ArrayLike

from . import Drift


class HeavisideDrift(Drift):
    _params = {"reject": 1.0}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __call__(self, points: ArrayLike, force_side: int = 0) -> ArrayLike:
        lags = self.fault(points, clip=False)
        drifts = np.full_like(
            lags, np.abs(self.reject)
        )  # make sure the sign matches the side
        if self.extent < np.inf:
            drifts *= self.attenuation(points)
        drifts *= self.side(lags, force_side)
        return drifts
