# -*- coding: utf-8 -*-

import numpy as np
from numpy.typing import ArrayLike

from .covariance import Covariance
from .models import models as MODELS


def fit_covariance_model(
    points: ArrayLike,
    lag_max: float,
    nbins: int,
    values: ArrayLike = None,
    model: str = "auto",
    return_metric: bool = False,
) -> Covariance:
    """
    Generates a Covariance object, from sample dataset.

    Parameters
    ----------
    points : ArrayLike
        Points of the data
    lag_max : float
        Maximum distance between points to consider them in the computation
    nbins : int
        Number of points of the experimental variogram to compute
    values : ArrayLike, optional
        Values of the variable to estimate at points, by default None, takes the points last coordinates.
    model : str, optional
        Covariance type to fit ('gaussian', 'exponential', 'cubic' or 'spherical'), by default "auto"
    return_metric : bool, optional
        Return distance metric between fitted model and experimental variogram, by default False

    Returns
    -------
    Covariance
        Fitted covariance model.
    """
    if model.lower() == "auto":
        # loop over all known models and return the best fitting
        covariances, metrics = [], []
        for model in MODELS.values():
            covariance, metric = model.fit(points, lag_max, nbins, values)
            covariances.append(covariance)
            metrics.append(metric)
        idx = np.argmin(metrics)
        return (covariances[idx], metrics[idx]) if return_metric else covariances[idx]
    elif model.lower() not in MODELS:
        raise ValueError(
            f"invalid model '{model}', must be one of {['auto'] + list(MODELS)}"
        )
    else:
        covariance, metric = MODELS[model].fit(points, lag_max, nbins, values)
        return (covariance, metric) if return_metric else covariance
