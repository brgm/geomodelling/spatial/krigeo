# -*- coding: utf-8 -*-
from typing import Optional

import numpy as np
from numpy.typing import ArrayLike

from ..covariance import Covariance


class SphericalCovariance(Covariance):
    _params = {
        "range": 1.0,
        "sill": 1.0,
        "nugget": 0.0,
    }

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def __call__(self, lag: ArrayLike, norms: Optional[ArrayLike] = None) -> ArrayLike:
        if norms is not None:
            # TODO: implement anisotropic model
            raise NotImplementedError
        # Formula: nugget + sill * (1.5 * lag - 0.5 * lag ** 3) if lag < 1
        #          nugget + sill if lag >= 1
        result = np.ones_like(lag)
        within_range = lag < self.range
        lag = lag[within_range]
        result_in_range = (1.5 / self.range) * lag
        result_in_range += (-0.5 / self.range**3) * lag**3
        result[within_range] = result_in_range
        result *= self.sill
        if self.nugget != 0.0:
            result += self.nugget
        return result

    @staticmethod
    def guess_p0(lag, var):
        """
        Autoguess of parameters.
            range = half of the domain
            sill = mean of var
            nugget = var[0]

        Parameters
        ----------
        lag : _type_
            _description_
        var : _type_
            _description_
        """
        return np.max(lag) / 2, np.mean(var), var[0]
