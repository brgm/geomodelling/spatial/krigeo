# -*- coding: utf-8 -*-
from typing import Optional

import numpy as np
from numpy.typing import ArrayLike

from ..covariance import Covariance


class ExponentialCovariance(Covariance):
    _params = {
        "range": 1.0,
        "sill": 1.0,
        "nugget": 0.0,
        "scale": 1.0,
    }

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def __call__(self, lag: ArrayLike, norms: Optional[ArrayLike] = None) -> ArrayLike:
        if norms is not None:
            # TODO: implement anisotropic model
            raise NotImplementedError
        # Formula: nugget + sill * (1.0 - np.exp(-scale * lag / range))
        result = (-self.scale / self.range) * lag
        result = np.exp(result, out=result)
        result -= 1.0
        result *= -self.sill
        if self.nugget != 0.0:
            result += self.nugget
        return result

    @staticmethod
    def guess_p0(lag, var):
        """
        Autoguess of parameters.
            range = half of the domain
            sill = mean of var
            nugget = var[0]
            scale = 1

        Parameters
        ----------
        lag : _type_
            _description_
        var : _type_
            _description_
        """
        return np.max(lag) / 2, np.mean(var), var[0], 1.0
