Welcome to |project|'s documentation!
======================================

|description|

.. toctree::
   about

.. toctree::
   usage

.. toctree::
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
