# -*- coding: utf-8 -*-
# Configuration file for the Sphinx documentation builder.

import sys
from datetime import date
from pathlib import Path

sys.path.append(Path(__file__).parent.parent.resolve())

try:
    from tomllib import loads
except ImportError:
    from tomli import loads
finally:
    path = Path(__file__).parent / "../pyproject.toml"
    with path.open("r", encoding="utf-8") as f:
        toml = loads(f.read())

# -- Project information -----------------------------------------------------

project = toml["project"]["name"]
copyright = toml["project"].get("copyright", f"{date.today().year}, BRGM")
authors = toml["project"].get("authors")
release = toml["project"].get("version", "latest")
repository = toml["project"].get("repository")

master_doc = "index"

# dynamically change the plugin variables
rst_prolog = (
    f".. |project| replace:: {project}" + "\n"
    f".. |description| replace:: {toml['project'].get('description')}" + "\n"
    f".. |version| replace:: {'`' + release + '`'}"
)

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "myst_parser",
    "numpydoc",
    "sphinx_rtd_theme",
]
autosummary_generate = True

source_suffix = {".rst": "restructuredtext", ".md": "markdown"}
add_module_names = False

# -- Options for HTML output -------------------------------------------------
html_theme = "sphinx_rtd_theme"
html_theme_options = {
    "navigation_depth": 4,
    "includehidden": True,
    "titles_only": False,
}
myst_all_links_external = True
# myst_url_schemes = {
#     "path": f"{repository}/{{path}}",
# }
