Usage
=====

Kriging
-------

.. autoclass:: krigeo.SimpleKriging
.. autoclass:: krigeo.OrdinaryKriging
.. autoclass:: krigeo.UniversalKriging

Drifts
------

.. autofunction:: krigeo.build_fault
.. autofunction:: krigeo.build_drift
.. autofunction:: krigeo.build_network

Covariance
----------

.. autoclass:: krigeo.covariance_models
