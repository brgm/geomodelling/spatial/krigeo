# plainKrige


## Overview

This package provides tools for variable estimation via kriging. It is divided in three submodules :
- *kriging* which provides three possibilities for kriging :
  - Simple kriging
  - Ordinary kriging
  - Universal kriging (or kriging with external drift)
- *covariance* which provides standard covariance models :
  - Gaussian
  - Cubic
  - Exponential
  - Spherical

  It also provides possibility to add a custom covariance model.
- *drifts* which is a toolbox designed to deal with discontinuities, that can then be integrated in universal kriging. It allows to create drift functions associated with faults for applications in geosciences.

## Usage

A demo folder is provided to show a use case of this package. It contains a global demo ([global demo](demo/global_demo.ipynb)) that shows the whole workflow, from the import of data to the kriging, with the definition of covariance and drift models. For more details on variography and drifts, see [variography](demo/variography.ipynb) and [drifts](demo/fault_modeling.ipynb), that focus on *covariance* and *drift* submodules applied to this use case.

## Installation

Tagged versions of `krigeo` are deployed on the [BRGM nexus], which allows a simple:
```sh
pip install krigeo -i https://nexus.brgm.fr/repository/pypi-all/simple
```
One can also build it manually from gitlab:
```bash
pip install git+https://gitlab.brgm.fr/brgm/geomedelling/internal/krigeo
```
To locally build/install this package, use:
```sh
git clone https://gitlab.brgm.fr/brgm/geomedelling/internal/krigeo
cd krigeo
# regular install
python -m pip install .
# install as editable
python -m pip install -e .
# or package wheel
python -m pip wheel . -w ./dist --no-deps
```

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

## License
plainKrige is provided under the GNU General Public License (GPLv3) that can be found in the [LICENSE](LICENSE) file.
By using, distributing, or contributing to this project, you agree to the terms and conditions of this license.

[BRGM nexus]:https://nexus.brgm.fr/
