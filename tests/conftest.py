# -*- coding: utf-8 -*-

import numpy as np
import pytest


@pytest.fixture
def n():
    return 32


@pytest.fixture
def grid_points(n):
    x, y = np.mgrid[-1 : 1 : complex(0, n), -1 : 1 : complex(0, n)]
    return np.c_[x.flat, y.flat]


@pytest.fixture
def rng():
    return np.random.default_rng(42)


# @pytest.fixture
# def cube_points():
#     return np.mgrid[-1:1:complex(0,n), -1:1:complex(0,n), -1:1:complex(0,n)].reshape(-1,3)


@pytest.fixture
def sparse_samples(n, grid_points, rng):
    idx = rng.choice(len(grid_points), size=n, replace=False)
    return grid_points[idx]


@pytest.fixture
def single_fault():
    """Single vertical fault crossing the grid."""
    return np.asarray([[0, -1, 0], [0, 0, 0], [0, 0.5, 0], [0, 1, 0]], dtype=np.double)


@pytest.fixture
def fault_network():
    return [
        np.asarray([[0, -1, 0], [0, 0, 0], [0, 0.5, 0], [0, 1, 0]]),
        np.asarray([[0, 0.2, 0], [1, -0.5, 0]]),
        np.asarray([[0, -0.3, 0], [0.6, -0.3, 0]]),
    ]


@pytest.fixture
def crossing_faults():
    """Two faults crossing each other : one vertical, one horizontal"""
    return [
        np.asarray([[0, -1, 0], [0, 0, 0], [0, 0.5, 0], [0, 1, 0]]),
        np.asarray([[-1, 0.2, 0], [0, 0.2, 0], [1, 0.2, 0]]),
    ]
