# -*- coding: utf-8 -*-
import numpy as np

from krigeo import covariance_models, fit_covariance_model, variogram

foo = lambda xy: np.sin(np.linalg.norm(xy, axis=-1))
x = np.linspace(0, 1, 100)


def test_model_prototype():
    models = {}
    for name, model in covariance_models.items():
        # build default model
        models[name] = model()
        # call it
        models[name](x)


def test_experimental_variogram(sparse_samples):
    known_values = foo(sparse_samples)
    lags, gamma = variogram(
        sparse_samples, values=known_values, lag_max=10_000, nbins=10
    )


def test_model_autofit(sparse_samples):
    known_values = foo(sparse_samples)
    # fit model to data
    covariance = fit_covariance_model(
        sparse_samples, lag_max=10_000, nbins=10, values=known_values
    )
    # evaluate model
    covariance(x)
