# -*- coding: utf-8 -*-

from timeit import default_timer as timer

import gstools as gs
import numpy as np
from pykrige.ok import OrdinaryKriging as Other

from krigeo import OrdinaryKriging as Self
from krigeo import covariance_models

foo = lambda xy: np.sin(np.linalg.norm(xy, axis=-1))
covariance = covariance_models["exponential"](sill=1, range=1, nugget=0)


def test_coherence(sparse_samples, grid_points):
    known_sites = sparse_samples
    known_values = foo(known_sites)
    estimated_sites = grid_points

    self_res, self_var = Self(known_sites, known_values, covariance=covariance)(
        estimated_sites, True
    )

    other, other_var = Other(
        known_sites[..., 0],
        known_sites[..., 1],
        known_values,
        variogram_model=gs.Exponential(dim=2, var=1, len_scale=1, nugget=0),
        verbose=False,
        enable_plotting=False,
    ).execute("points", estimated_sites[..., 0], estimated_sites[..., 1])

    assert np.allclose(self_res, other), "divergent results"
    assert np.allclose(self_var, other_var), "divergent results"


def test_performance(sparse_samples, grid_points):
    known_sites = sparse_samples
    known_values = foo(known_sites)
    estimated_sites = grid_points

    tic = timer()
    Self(known_sites, known_values, covariance=covariance)(estimated_sites, True)
    self = timer() - tic

    tic = timer()
    Other(
        known_sites[..., 0],
        known_sites[..., 1],
        known_values,
        variogram_model=gs.Exponential(dim=2),
        verbose=False,
        enable_plotting=False,
    ).execute("points", estimated_sites[..., 0], estimated_sites[..., 1])
    other = timer() - tic

    assert self < other, "slower implementation"
