# -*- coding: utf-8 -*-

import numpy as np

from krigeo import (
    UniversalKriging,
    build_drift,
    build_fault,
    build_network,
    covariance_models,
)

foo = lambda xy: -((np.linalg.norm(xy, axis=-1)) ** 2)
covariance = covariance_models["exponential"](sill=1, range=1, nugget=0)


def test_single_fault(sparse_samples, grid_points, single_fault):
    functor = build_fault(single_fault)
    assert np.all(
        functor(single_fault) == 0
    ), "Distance should be zero along fault trace"

    drifts = [build_drift(functor, model="heaviside", extent=np.inf)]
    assert len(drifts) == 1

    computed_drift = drifts[0](grid_points)
    assert np.all(
        np.where(
            (computed_drift == 0.0)
            | (computed_drift == 1.0)
            | (computed_drift == -1.0),
            True,
            False,
        )
    ), "Drifts functions should return only 0/-1/1 when model is Heaviside"

    known_values = foo(sparse_samples)
    estimator = UniversalKriging(
        sparse_samples,
        lag_max=10_000,
        nbins=10,
        values=known_values,
        covariance=covariance,
        drifts=drifts,
    )

    assert np.allclose(
        estimator(sparse_samples), known_values
    ), "estimation should be exact at data points"


def test_crossing_faults(crossing_faults):
    drifts = build_network(crossing_faults, buffer=0.1)
    matrix = drifts.matrix

    assert np.all(matrix == np.asarray([[0, 0], [0, 0]]))
    assert len(drifts) == 2, "Drifts does not have the right shape."


def test_multiple_faults(fault_network):
    drifts = build_network(fault_network, buffer=0.1)
    matrix = drifts.matrix

    assert np.all(matrix == np.asarray([[0, 0, 0], [-1, 0, 0], [-1, -1, 0]]))
    assert len(drifts) == 3, "Drifts does not have the right shape."


def test_models(single_fault, grid_points):
    fault = build_fault(single_fault)

    finite_heaviside = build_drift(fault, model="heaviside")(grid_points)
    infinite_heaviside = build_drift(fault, model="heaviside", extent=np.inf)(
        grid_points
    )

    finite_gaussian = build_drift(fault, model="gaussian", range=0.2)(grid_points)
    infinite_gaussian = build_drift(fault, model="gaussian", range=0.2, extent=np.inf)(
        grid_points
    )


def test_relations():
    main = build_fault(points=[(0, 1), (0, -1)])
    left = build_fault(points=[(-1, 0.5), (-0.1, 0.5)])
    right = build_fault(points=[(1, 0.5), (0.1, 0.5)])

    faults = [main, left, right]

    net = build_network(faults)

    points = [(-0.25, -0.25), (0.25, -0.25), (-0.25, 0.25), (0.25, 0.25)]

    net(points)
