# -*- coding: utf-8 -*-
import os


def test_variography():
    os.system(f"jupyter nbconvert --to notebook --execute ./demo/variography.ipynb")


def test_global():
    os.system(f"jupyter nbconvert --to notebook --execute ./demo/global_demo.ipynb")


def test_drifts():
    os.system(f"jupyter nbconvert --to notebook --execute ./demo/fault_modeling.ipynb")
