# -*- coding: utf-8 -*-

import numpy as np

from krigeo import OrdinaryKriging, UniversalKriging

foo = lambda xy: np.sin(np.linalg.norm(xy, axis=-1))


# def test_simple_kriging(sparse_samples, grid_points):
#     known_sites = sparse_samples
#     known_values = foo(known_sites)
#     estimator = SimpleKriging(known_sites, known_values, covariance="auto")

#     estimated_sites = grid_points
#     estimated_values, estimated_variance = estimator(
#         estimated_sites, return_variance=True
#     )

#     # basic "shape" tests
#     assert len(estimated_values) == len(estimated_sites), "oups something went wrong"
#     assert len(estimated_values) == len(estimated_variance), "oups something went wrong"

#     # kriging should be exact at "training" points
#     assert np.allclose(
#         estimator(known_sites), known_values
#     ), "estimation should be exact at data points"

#     # variance should always be positive
#     assert np.all(estimated_variance >= 0), "Variance should be positive"


def test_ordinary_kriging(sparse_samples, grid_points):
    known_sites = sparse_samples
    known_values = foo(known_sites)
    estimator = OrdinaryKriging(
        known_sites, lag_max=10_000, nbins=10, values=known_values, covariance="auto"
    )

    estimated_sites = grid_points
    estimated_values, estimated_variance = estimator(
        estimated_sites, return_variance=True
    )

    # basic "shape" tests
    assert len(estimated_values) == len(estimated_sites), "oups something went wrong"
    assert len(estimated_values) == len(estimated_variance), "oups something went wrong"

    # The sum of Ke coefficients except last should equal 1
    coef_sum = np.zeros(len(estimated_sites))
    w = estimator.iK @ estimator.Ke(estimated_sites)

    for i in range(len(estimated_sites)):
        coef_sum[i] = np.sum(w[:-1, i])
    assert np.allclose(coef_sum, np.ones(len(estimated_sites)))

    # kriging should be exact at "training" points
    assert np.allclose(
        estimator(known_sites), known_values
    ), "estimation should be exact at data points"

    # variance should always be positive
    assert np.all(estimated_variance >= 0), "Variance should be positive"


def test_universal_kriging(sparse_samples, grid_points):
    known_sites = sparse_samples
    known_values = foo(known_sites)
    drift = np.vectorize(lambda pt: 1.0 if pt[0] > 0.0 else -1.0, signature="(d)->()")

    estimator = UniversalKriging(
        known_sites,
        lag_max=10_000,
        nbins=10,
        values=known_values,
        covariance="auto",
        drifts=[drift],
    )

    estimated_sites = grid_points
    estimated_values, estimated_variance = estimator(
        estimated_sites, return_variance=True
    )

    # basic "shape" tests
    assert len(estimated_values) == len(estimated_sites), "oups something went wrong"
    assert len(estimated_values) == len(estimated_variance), "oups something went wrong"

    # The sum of Ke coefficients except last should equal 1
    coef_sum = np.zeros(len(estimated_sites))
    w = estimator.iK @ estimator.Ke(estimated_sites)

    for i in range(len(estimated_sites)):
        coef_sum[i] = np.sum(w[:-2, i])
    assert np.allclose(coef_sum, np.ones(len(estimated_sites)))

    # kriging should be exact at "training" points
    assert np.allclose(
        estimator(known_sites), known_values
    ), "estimation should be exact at data points"

    # variance should always be positive
    assert np.all(estimated_variance >= 0), "Variance should be positive"
